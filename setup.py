# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open("README.md") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = [
    "click>=7.0",
    "ldap3>=2.6.1",
    "deepdiff>=4",
    "click-log>=0.3.0",
    "cerberus>=1.3.1",
]

test_requirements = [
    "pytest",
    "pytest-cov",
    "responses",
    "coverage",
    "pytest-logger",
    "pre-commit",
]

setup(
    name="ldap-feeder",
    author="Alessio Curri",
    author_email="alessio.curri@esss.se",
    description="LDAP manipulation tool",
    long_description=readme + "\n\n" + history,
    url="https://gitlab.esss.lu.se/ics-infrastructure/ldap-feeder",
    license="MIT license",
    version="0.1.3",
    install_requires=requirements,
    test_suite="tests",
    tests_require=test_requirements,
    packages=find_packages(),
    include_package_data=True,
    keywords="ldap",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
    ],
    entry_points={"console_scripts": ["ldap-feeder=ldap_feeder.__main__:cli"]},
)
