# Ldap-Feeder
Ldap-Feeder is a python3 CLI tool that allows the manipulation of an LDAP (sub)tree.
Different sub-modules are in charge of different kind of structures, like sudoers objects and NisNetGroups.

## Usage
```
Usage: ldap-feeder [OPTIONS]

Options:
  --version                       Show the version and exit.
  -i, --inputfile PATH            [required]
  -c, --configfile PATH           [required]
  --mockserver / --no-mockserver  Use an internal ldap server. For testing
                                  only.
  -v, --verbosity LVL             Either CRITICAL, ERROR, WARNING, INFO or
                                  DEBUG
  --help                          Show this message and exit.
```

The ```mockserver``` option instructs the ldap-feeder not to actually connect to any real LDAP server, and to create a mock object in memory.
The CLI interface ```ldap-feeder``` requires a config file (JSON) and an input file (JSON again).

## Configuration

The config file must contain, obviously, the configuration parameters, like target LDAP host, the basedn  and the authentication details.
Here is an example for the configuration file:
```json
{
    "basedn": "o=ESS",
    "host": "host.lan",
    "password": "pass",
    "user": "dn=user,ou=Users,o=ESS",
    "use_tls": true
}
```
The user must be allowed to search, delete, and create objects inside the specified basedn.

## Inputfile
The JSON input file must contain an dictionary with one element per module (see later).
Only if the corresponding module name is found a module is activated.
Different modules require different structures depending on the scope of the module itself.

Below an example inputfile for the ```nisnetgroups``` and ```sudoers``` modules.
```json
{
    "nisnetgroups": {
        "group1": [
            "host1.tn",
            "host2.tn",
            "host5.tn",
            "host6.tn",
            "host7.tn",
            "host8.tn",
            "host9.tn"
        ],
        "group2": [
            "host3.cslab",
            "host4.cslab",
            "host11.cslab",
            "host12.cslab",
            "host13.cslab"
        ],
        "group3": [
            "host3.ess",
            "host4.ess",
            "host11.ess",
            "host12.ess",
            "host13.ess"
        ]
    },
    "sudoers": {
        "group1": [
            {
                "user": "stephane"
            },
            {
                "commands": [
                    "/sbin/reboot"
                ],
                "user": "han"
            },
            {
                "user": "amir"
            }
        ],
        "group2": [
            {
                "user": "alessio"
            },
            {
                "user": "stephane"
            }
        ],
        "group3": [
            {
                "commands": [
                    "/bin/bash",
                    "/bin/id"
                ],
                "user": "amir"
            },
            {
                "user": "han"
            }
        ],
        "group4": [
            {
                "user": "csi"
            }
        ]
    }
}
```

## NisNetgroups module
This module creates and manages ```objectClass=nisNetgroup``` objects containing multiple ```nisNetgroupTriple``` attributes.
The inputfile must contain a ```nisnetgroups``` entry with the parameters (none atm, use an empty string) and under ```core``` it must contain a dictionary where the keys are group names and the corresponding attribute is a list of hosts.

## Sudoers module
This module creates and manages ```objectClass=sudoRole``` objects.
The inputfile, under ```nisnetgroups```, must contain a dictionary where the keys are NIS NetGroups (you can, and should, use groups defined by the ```NisNetGroups``` module).
The attribute must be a list of dictionaries. See the above example.
