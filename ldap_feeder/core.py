import logging
import click_log
from cerberus import Validator
from ldap3 import Server, Connection, MOCK_SYNC, SYNC
from ldap3 import SUBTREE, ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES
from ldap3.core.exceptions import LDAPSocketOpenError
from .sudoers import populate as sudoers_populate
from .nisnetgroups import populate as nisnetgroups_populate
from .errors import FeederConnectionException, FeederConfigException

log = logging.getLogger("ldap-feeder")
click_log.basic_config(log)


class LdapFeeder:
    MODULES_MAP = {"nisnetgroups": nisnetgroups_populate, "sudoers": sudoers_populate}

    CONFIG_SCHEMA = {
        "user": {"type": "string", "required": True},
        "password": {"type": "string", "required": True},
        "host": {"type": "string", "required": True},
        "basedn": {"type": "string", "required": True},
        "use_tls": {"type": "boolean", "required": True},
    }

    def __init__(self, config, mockserver, modules_args):
        self._mockserver = mockserver
        config_validator = Validator(self.CONFIG_SCHEMA)
        if not config_validator(config):
            raise FeederConfigException("Invalid config")
        self._config = config
        self._modules_args = modules_args
        self._server = Server(self._config["host"])

    def _get_ldap_connection(self):
        if self._mockserver:
            strategy = MOCK_SYNC
        else:
            strategy = SYNC
        conn = Connection(
            self._server,
            user=self._config["user"],
            password=self._config["password"],
            client_strategy=strategy,
        )
        try:
            if self._config["use_tls"]:
                conn.start_tls()
            if not conn.bind():
                description = "Unknown error during LDAP connection"
                if conn.result["description"] == "invalidCredentials":
                    description = (
                        "Cannot bind with the LDAP server: Invalid Credentials"
                    )
                elif conn.result["description"] == "confidentialityRequired":
                    description = "Cannot bind with the LDAP server: TLS required"
                log.debug("Cannot bind, bailing")
                raise FeederConnectionException(description)
        except LDAPSocketOpenError as e:
            log.debug("LDAPSocketOpenError => %s", e)
            raise FeederConnectionException(
                "Cannot establish a connection with the LDAP server"
            )

        return conn

    def _create_ou(self, conn, ou, basedn=None):
        if basedn is None:
            basedn = self._config["basedn"]
        dn = "ou={},{}".format(ou, basedn)
        conn.search(dn, "(objectclass=organizationalUnit)")
        if len(conn.entries) == 0:
            log.info("Adding ou=%s", dn)
            if not conn.add(dn, "organizationalUnit"):
                log.error("Cannot add entry, entry => %s", dn)
        elif len(conn.entries) > 1:
            log.error("Multiple entries in creating ou=%s", dn)
        elif len(conn.entries) == 1:
            log.debug("Uo already exists, skipping dn=%s", dn)

    def feed(self):
        for key in self._modules_args:
            if key in self.MODULES_MAP.keys() and not key == "core":
                log.info("Data for module %s found, running", key)
                conn = self._get_ldap_connection()
                self._create_ou(conn, key)
                baseou = "ou={},{}".format(key, self._config["basedn"])
                conn_helper = LdapFeederHelper(conn, baseou)
                self.MODULES_MAP[key](
                    conn=conn_helper,
                    data=self._modules_args[key],
                    common_data=self._modules_args["core"],
                )
                # TODO: unbind the ldap connection
            else:
                log.error("Module %s not found", key)


class LdapFeederHelper:
    def __init__(self, conn, basedn, base_attribute="cn"):
        self._conn = conn
        self._basedn = basedn
        self._base_attribute = base_attribute

    def search(
        self,
        filter,
        scope=SUBTREE,
        attributes=[ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES],
    ):
        self._conn.search(
            search_base=self._basedn,
            search_filter=filter,
            search_scope=scope,
            attributes=attributes,
        )
        return list(self._conn.entries)

    def add(self, name, object_class, attributes):
        if isinstance(object_class, list) and "top" not in object_class:
            object_class.append("top")
        else:
            object_class = ["top", object_class]
        dn = "{}={},{}".format(self._base_attribute, name, self._basedn)
        if not self._conn.add(dn, object_class, attributes):
            log.error("Cannot add %s, result => %s", dn, self._conn.result)

    def delete(self, name):
        dn = "{}={},{}".format(self._base_attribute, name, self._basedn)
        if not self._conn.delete(dn):
            log.error("Cannot delete %s, result => %s", dn, self._conn.result)
