import logging
from deepdiff import DeepDiff


log = logging.getLogger("ldap-feeder")


def populate(conn, data, common_data):
    parsed = {}
    for groupname in data.keys():
        rules = data[groupname]
        for rule in rules:
            user = rule["user"]
            commands = rule.get("commands", ["ALL"])
            if groupname in common_data["groups"]:
                for host in common_data["groups"][groupname]:
                    userEntity = parsed.get(user, {})
                    userEntity["{}".format(host)] = list(commands)
                    parsed[user] = userEntity

    actual = {}

    search = conn.search(filter="(objectClass=sudoRole)")
    for entry in search:
        user = str(entry["sudoUser"])
        userEntity = actual.get(user, {})
        userEntity[str(entry["sudoHost"])] = list(entry["sudoCommand"])
        actual[user] = userEntity

    to_be_removed = list(set(actual.keys()) - set(parsed.keys()))
    to_be_added = list(set(parsed.keys()) - set(actual.keys()))

    # To change an entry it's simpler to delete and recreate
    for entry in parsed.keys():
        if actual.get(entry, None) is not None:
            diff = DeepDiff(actual[entry], parsed[entry])
            if diff != {}:
                log.info("User %s had some changes, deleting and reading", entry)
                log.debug("User %s was => %s", entry, diff)
                to_be_removed.append(entry)
                to_be_added.append(entry)

    for entry in to_be_removed:
        for sudohost in actual[entry]:
            usercn = "{}-{}".format(entry, sudohost)
            log.info("Removing sudorole for user %s on %s", usercn, sudohost)
            conn.delete(usercn)

    for entry in to_be_added:
        for sudohost in parsed[entry]:
            usercn = "{}-{}".format(entry, sudohost)
            log.info("Adding sudorole for user %s on %s", usercn, sudohost)
            conn.add(
                name=usercn,
                object_class="sudoRole",
                attributes={
                    "sudoUser": entry,
                    "sudoCommand": parsed[entry][sudohost],
                    "sudoHost": sudohost,
                },
            )

    log.debug("Parsed => %s", parsed)
    log.debug("Actual => %s", actual)
    log.debug("Removed => %s", to_be_removed)
    log.debug("Added => %s", to_be_added)
