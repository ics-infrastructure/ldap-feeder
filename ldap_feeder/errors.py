class FeederException(Exception):
    pass


class FeederConnectionException(FeederException):
    pass


class FeederConfigException(FeederException):
    pass
