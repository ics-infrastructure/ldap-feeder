#!/usr/bin/env python
# Copyright (c) 2019 European Spallation Source ERIC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import logging
import sys
import json
import click
import click_log
from .core import LdapFeeder as feeder
from .errors import FeederConnectionException, FeederConfigException


log = logging.getLogger("ldap-feeder")
click_log.basic_config(log)


@click.command()
@click.version_option()
@click.option("-i", "--inputfile", type=click.Path(), required=True)
@click.option("-c", "--configfile", type=click.Path(), required=True)
@click.option(
    "--mockserver/--no-mockserver",
    default=False,
    help="Use an internal ldap server. For testing only.",
)
@click_log.simple_verbosity_option(log)
def cli(configfile, mockserver, inputfile):
    try:
        log.debug("Loading input file %s", inputfile)
        with open(inputfile, "r") as f:
            data = json.load(f)
    except ValueError:
        data = {}
        log.error("Cannot parse input file %s", inputfile)
        sys.exit(1)
    try:
        log.debug("Loading config file %s", configfile)
        with open(configfile, "r") as f:
            config = json.load(f)
    except ValueError:
        config = {}
        log.error("Cannot parse input file %s", configfile)
        sys.exit(2)
    try:
        myfeeder = feeder(config=config, modules_args=data, mockserver=mockserver)
        myfeeder.feed()
    except FeederConnectionException as e:
        log.error(e)
        sys.exit(3)
    except FeederConfigException as e:
        log.error(e)
        sys.exit(4)
