import logging


log = logging.getLogger("ldap-feeder")


def populate(conn, data, common_data):
    parsed = {}
    for groupname in common_data["groups"].keys():
        if groupname == "all" or len(common_data["groups"][groupname]) < 1:
            continue
        triplets = []
        for host in common_data["groups"][groupname]:
            triplets.append("({},,)".format(host))
        parsed[groupname] = sorted(triplets)

    actual = {}
    search = conn.search(filter="(objectClass=nisNetgroup)")
    for entry in search:
        mydn = str(entry.cn)
        actual[mydn] = sorted(list(entry.nisNetgroupTriple))

    to_be_removed = list(set(actual.keys()) - set(parsed.keys()))
    to_be_added = list(set(parsed.keys()) - set(actual.keys()))

    # To change an entry it's simpler to delete and recreate
    for entry in parsed.keys():
        if actual.get(entry, None) is not None:
            if not actual[entry] == parsed[entry]:
                log.info("Group %s had some changes, deleting and readding", entry)
                to_be_removed.append(entry)
                to_be_added.append(entry)

    for entry in set(to_be_removed):
        log.info("Removing group dn=%s", entry)
        conn.delete(entry)

    for entry in set(to_be_added):
        log.info("Adding group dn=%s", entry)
        conn.add(
            name=entry,
            object_class="nisNetgroup",
            attributes={"nisNetgroupTriple": parsed[entry]},
        )

    log.debug("Parsed => %s", parsed)
    log.debug("Actual => %s", actual)
    log.debug("Removed => %s", to_be_removed)
    log.debug("Added => %s", to_be_added)
