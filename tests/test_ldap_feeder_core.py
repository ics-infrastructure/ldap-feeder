from ldap_feeder.core import LdapFeeder as feeder
import logging


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def test_ldap_feeder_nisnetgroups(fake_config, caplog, modules_args_netgroup):
    caplog.set_level(logging.INFO)
    modules_args, hosts1, hosts2, hosts3, hosts4 = modules_args_netgroup

    # First run                    }
    myfeeder = feeder(config=fake_config, mockserver=True, modules_args=modules_args)
    myfeeder.feed()

    # Change the data and rerun, and rerun
    modules_args["core"].pop("group3", None)
    modules_args["core"]["group4"] = hosts4
    myfeeder.feed()

    # Change a list. this will be nuked and regenerated
    hosts1.pop()
    myfeeder.feed()


def test_ldap_feeder_sudoers(fake_config, caplog, modules_args_sudoers):
    caplog.set_level(logging.INFO)
    modules_args, sudoers1, sudoers2, sudoers3, sudoers4 = modules_args_sudoers

    myfeeder = feeder(config=fake_config, mockserver=True, modules_args=modules_args)
    log.info("First Run")
    myfeeder.feed()
    log.info("Second Run")
    modules_args["sudoers"].pop("group4", None)
    myfeeder.feed()
    log.info("Third Run")
    modules_args["sudoers"]["group1"].pop()
    myfeeder.feed()
    log.info("Fourth Run")
    myfeeder.feed()
