import pytest
import json


@pytest.fixture(scope="function")
def modules_args_netgroup():
    hosts1 = [
        "host1.tn",
        "host2.tn",
        "host5.tn",
        "host6.tn",
        "host7.tn",
        "host8.tn",
        "host9.tn",
    ]
    hosts2 = [
        "host3.cslab",
        "host4.cslab",
        "host11.cslab",
        "host12.cslab",
        "host13.cslab",
    ]
    hosts3 = ["host3.ess", "host4.ess", "host11.ess", "host12.ess", "host13.ess"]
    hosts4 = ["host3.fake", "host4.fake", "host11.fake", "host12.fake", "host13.fake"]
    modules_args = {
        "nisnetgroups": {},
        "core": {"groups": {"group1": hosts1, "group2": hosts2, "group3": hosts3}},
    }
    return (modules_args, hosts1, hosts2, hosts3, hosts4)


@pytest.fixture(scope="function")
def modules_args_sudoers():
    sudoers1 = [
        {"user": "stephane"},
        {"user": "han", "commands": ["/sbin/reboot"]},
        {"user": "amir"},
    ]
    hosts1 = [
        "host1.tn",
        "host2.tn",
        "host5.tn",
        "host6.tn",
        "host7.tn",
        "host8.tn",
        "host9.tn",
    ]
    sudoers2 = [{"user": "alessio"}, {"user": "stephane"}]
    sudoers3 = [{"user": "amir", "commands": ["/bin/bash", "/bin/id"]}, {"user": "han"}]
    sudoers4 = [{"user": "csi"}]
    modules_args = {
        "sudoers": {
            "group1": sudoers1,
            "group2": sudoers2,
            "group3": sudoers3,
            "group4": sudoers4,
        },
        "core": {"groups": {"group1": hosts1}},
    }
    return (modules_args, sudoers1, sudoers2, sudoers3, sudoers4)


@pytest.fixture(scope="function")
def fake_csentry_file(tmpdir_factory, modules_args_sudoers, modules_args_netgroup):
    modules_args1, sudoers1, sudoers2, sudoers3, sudoers4 = modules_args_sudoers
    modules_args2, hosts1, hosts2, hosts3, hosts4 = modules_args_netgroup
    data = {}
    data["sudoers"] = modules_args1["sudoers"]
    data["nisnetgroups"] = ""
    data["core"] = {}
    data["core"] = modules_args2["core"]
    print(data)
    fn = tmpdir_factory.mktemp("data").join("file.json")
    with open(fn, "w") as f:  # writing JSON object
        json.dump(data, f, sort_keys=True, indent=4)
    return fn


@pytest.fixture(scope="function")
def fake_config():
    config = {
        "user": "",
        "password": "",
        "host": "ahost.lan",
        "basedn": "o=ESS",
        "use_tls": False,
    }
    return config


@pytest.fixture(scope="function")
def fake_config_file(tmpdir_factory, fake_config):
    fn = tmpdir_factory.mktemp("data").join("config.json")
    with open(fn, "w") as f:  # writing JSON object
        json.dump(fake_config, f, sort_keys=True, indent=4)
    return fn
