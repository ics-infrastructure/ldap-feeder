# -*- coding: utf-8 -*-
from click.testing import CliRunner
import pytest
from ldap_feeder.__main__ import cli as mycli


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


def test_cli(runner, fake_csentry_file, fake_config_file):
    result = runner.invoke(
        mycli,
        [
            "--mockserver",
            "--configfile",
            fake_config_file,
            "--inputfile",
            str(fake_csentry_file),
        ],
    )
    assert result.exit_code == 0
